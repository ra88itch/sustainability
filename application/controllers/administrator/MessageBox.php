<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		redirect('administrator/messageBox/lists');
	}
	
	function lists($index = 1)
	{
		$response = $this->config->item('response');
		$this->load->view('administrator/messageBox/lists', $response);
	}
	
	function view($id = '')
	{
		$response = $this->config->item('response');
		$this->load->view('administrator/messageBox/view', $response);
	}
}
