<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo2 extends CI_Controller {

	function __construct() {
        parent::__construct();

		// load library
		$this->load->model('dummy_data_model', '', TRUE);
	}

	public function index(){			
		$response = $this->config->item('response');
		$response['nav_active'] = 'home';
		$response['js'] = ['js/lottie'];
		$response['css'] = ['main_page','video','banner','topic-box', 'project', 'news'];
		
		$response['data'] = $this->dummy_data_model->home_page();

		$this->load->view('starter/page', $response);
	}

	function policy(){
		$response = $this->config->item('response');
		$response['nav_active'] = 'policy';
		$response['css'] = ['policy_page','banner', 'topic-box','project','news'];
		$this->load->view('starter/page',$response);	
	}

	function project(){
		$response['nav_active'] = 'project';
		$response['css'] = ['project_page','banner'];
		$this->load->view('starter/page',$response);	
	}

	function service(){
		$response['nav_active'] = 'service';
		$response['css'] = ['service_page','video','download','interactive','banner'];
		$this->load->view('starter/page',$response);	
	}
}
