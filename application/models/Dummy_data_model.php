<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dummy_data_model extends CI_Model {

    function __construct() {
        parent::__construct();
		/* $module_type = array(
			'banner', // Done
			'text', // Done
			'feature-article',
			'feature-youtube', 
			'feature-project', 
			'feature-product', 
			'feature-blog', 
			'feature-contact', 
			'timeline');
		*/
    }

	function home_page(){
		$return = array();
		$return[] = $this->banner_lottie();
		$return[] = $this->text();

		
		$return = json_encode($return);
		$return = json_decode($return);
		return $return;
	}







































	private function banner_lottie(){
		$content = array(
			'en' => array(
				'title' => 'DTAC<br>Sustainablity',
				'title_color' => '', // Default #000
				'description' => '',
				'description_color' => '', // Default #000
				'link' => '#',
				'link_text' => 'Read more',
				'banner_type' => 'lottie', // image, lottie
				'banner_file' => 'main.json'
			),
			'th' => array(
				'title' => 'ดีแทค<br>กับความยั่งยืน',
				'title_color' => '', // Default #000
				'description' => 'สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร',
				'description_color' => '', // Default #000
				'link' => '#',
				'link_text' => 'อ่านเพิ่ม',
				'banner_type' => 'lottie', // image, lottie
				'banner_file' => 'main.json'
			)
		);
		
		$content = json_encode($content);
		$content = json_decode($content);
		$content = json_encode($content);
		
		$return = array(
			'id' => '1',
			'name' => '',
			'content' => $content,
			'module_type' => 'banner',
			'full_width' => false, // TRUE/FALSE
			'bg_color' => '', // Default transparent
			'bg_image' => '' // 
		);
		
		return $return;
	}

	private function banner_image(){
		$content = array(
			'en' => array(
				'title' => 'DTAC<br>Sustainablity',
				'title_color' => '', // Default #000
				'description' => '',
				'description_color' => '', // Default #000
				'link' => '',
				'link_text' => 'Read more',
				'banner_type' => 'image', // image, lottie
				'banner_file' => 'main.png'
			),
			'th' => array(
				'title' => 'ดีแทค<br>กับความยั่งยืน',
				'title_color' => '', // Default #000
				'description' => 'สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร',
				'description_color' => '', // Default #000
				'link' => '#',
				'link_text' => 'อ่านเพิ่ม',
				'banner_type' => 'lottie', // image, lottie
				'banner_file' => 'image.png'
			)
		);
		$content = json_encode($content);
		$content = json_decode($content);
		$content = json_encode($content);

		$return = array(
			'id' => '1',
			'name' => '',
			'content' => $content,
			'module_type' => 'banner',
			'full_width' => true, // TRUE/FALSE,
			'bg_color' => '', // Default transparent
			'bg_image' => '' // 
		);
		
		return $return;
	}

	private function text(){
		$content = array(
			'en' => array(
				'title' => 'Smart Farmer',
				'title_color' => '', // Default #000
				'description' => 'สร้างความยั่งยืนให้แก่ประเทศชาติ ดีแทคได้ยึดถือหลักการ `ทำในสิ่งที่ถูกต้อง` หรือ Do the Right Thing ควบคู่ไปกับการ `สร้างคุณค่ารวม` หรือ Creating Shared Value (CSV) ให้แก่สั่งคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค `สร้างไทยให้แกร่ง` และมุ่งสู่เป้าหมายอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ',
				'description_color' => '', // Default #000
				'link' => '',
				'link_text' => 'Read more'
			),
			'th' => array(
				'title' => 'กลยุทธ์การพัฒนาอย่างยั่งยืนของเรา',
				'title_color' => '', // Default #000
				'description' => 'สร้างความยั่งยืนให้แก่ประเทศชาติ ดีแทคได้ยึดถือหลักการ `ทำในสิ่งที่ถูกต้อง` หรือ Do the Right Thing ควบคู่ไปกับการ `สร้างคุณค่ารวม` หรือ Creating Shared Value (CSV) ให้แก่สั่งคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค `สร้างไทยให้แกร่ง` และมุ่งสู่เป้าหมายอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ',
				'description_color' => '', // Default #000
				'link' => '',
				'link_text' => 'อ่านเพิ่ม'
			)
		);
		$content = json_encode($content);
		$content = json_decode($content);
		$content = json_encode($content);

		$return = array(
			'id' => '1',
			'name' => '',
			'content' => $content,
			'module_type' => 'text',
			'full_width' => true, // TRUE/FALSE,
			'bg_color' => '', // Default transparent
			'bg_image' => '' // 
		);
		
		return $return;
	}

}
