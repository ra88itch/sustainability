<?php
$this->load->view('starter/_head');
$this->load->view('starter/_nav');

foreach($data AS $content_module){
	//$starter['root'] = $root;
	$starter['module'] = $content_module;
	$this->load->view('starter/module/'.$content_module->module_type, $starter);
}

$this->load->view('starter/_footer');
?>