<?php
$module_content = json_decode($module->content);

$content = $module_content->th;
if(isset($lang) && $lang == 'en'){ 
	$content = $module_content->en;
}
?>
<section class="container-fluid module-text" id="module-text-<?php echo $module->id; ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12 text-center">
				<h2 class=""><strong><?php echo $content->title; ?></strong></h2>
				<p><?php echo $content->description; ?></p>
				<?php
				if($content->link != ''){
					echo '<a href="'.$content->link.'" class="btn btn-outline-dark btn-radius70">'.$content->link_text.'</a>'; 
				} ?>
			</div>
		</div>
	</div>
</section>