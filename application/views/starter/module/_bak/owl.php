<script type="text/javascript">
	var owl = $('.news .owl-carousel');
	owl.owlCarousel({
		// center:true,
		items:4,
		loop:true,
		margin:10,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		smartSpeed:1800,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				// nav:true,
				loop:true
			},
			600:{
				items:2,
				// nav:false,
				loop:true
			},
			1000:{
				items:4,
				// nav:true,
				loop:true
			},
			3000:{
				items:8,
				// nav:true,
				loop:true
			}
		}
	});

	var owl = $('.toppic .owl-carousel');
	owl.owlCarousel({
		center:true,
		items:1,
		loop:true,
		margin:10,
		autoplay:false,
		// autoplayTimeout:2000,
		// autoplayHoverPause:true,
		smartSpeed:800,
		
	});

	var owl_project = $('.project .owl-carousel');
	owl_project.owlCarousel({
		items:1,
		loop:true,
		margin:10,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		smartSpeed:1800,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				// nav:true,
				loop:true
			}
		}

		
	});

	var owl_project = $('.youtube .owl-carousel');
	owl_project.owlCarousel({
		center:true,
		items:1,
		loop:true,
		margin:10,
		autoplay:false,
		// autoplayTimeout:2000,
		// autoplayHoverPause:true,
		smartSpeed:800,
		
	});
</script>