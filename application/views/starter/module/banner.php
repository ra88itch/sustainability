<?php
$module_content = json_decode($module->content);

$content = $module_content->th;
if(isset($lang) && $lang == 'en'){ 
	$content = $module_content->en;
}
?>
<section class="container-fluid module-banner" id="module-banner-<?php echo $module->id; ?>">
	<div class="<?php if($module->full_width == true){ echo 'container'; }?>">
		<div class="row">

			<div class="col-xl-5 col-lg-12">
				<h1 class="dtac-bold oversize-xxl"><?php echo $content->title; ?></h1>
				<p><?php echo $content->description; ?></p>
				<?php
				if($content->link != ''){
					echo '<a href="'.$content->link.'" class="btn btn-outline-dark btn-radius70">'.$content->link_text.'</a>'; 
				} ?>
			</div>

			<div class="col-xl-7 col-md-12">				
				<?php $html = '';				
				if($content->banner_file != ''){
					switch($content->banner_type){
						case 'lottie':
							$script = 'lottie.loadAnimation( {
								container: document.getElementById("lottie-'.$module->id.'"), 
								renderer: "svg",
								loop: true,
								autoplay: true,
								path: "'.$root.'assets/lottie/'.$content->banner_file.'" 
							});'; 
							$html = '<div id="lottie-'.$module->id.'"></div><script>'.$script.'</script>';
							break;
						case 'image':
						default:
							$html = '<img class="" src="'.$root.'upload/'.$content->banner_file.'">';
							break;
					}
				}
				echo $html;
				?>
			</div>

		</div>
	</div>
</section>
