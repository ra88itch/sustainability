<?php
// Prototype $menu
if($demo == true){
$menu = array(
	array(
		'id' => '1',
		'alias'=>'home',
		'link'=>'',
		'title'=>array(
			'en'=>'Home',
			'th'=>'ภาพรวม'
		),
		'link_type'=>'alias', // alias, dropdown, external default is alias
		'dropdown'=>array(),
		'home' => TRUE
	),
	array(
		'id' => '3',
		'alias'=>'policy',
		'link'=>'policy',
		'title'=>array(
			'en'=>'Policy',
			'th'=>'เราคิดอย่างไร'
		),
		'link_type'=>'alias',
		'dropdown'=>array(),
		'home' => FALSE
	),
	array(
		'id' => '4',
		'alias'=>'project',
		'link'=>'',
		'title'=>array(
			'en'=>'Project',
			'th'=>'สิ่งที่เราทำ'
		),
		'link_type'=>'dropdown',
		'dropdown'=> array(
			0 => array(
				'id' => '9',
				'alias'=>'projectOverall',
				'link'=>'project',
				'title'=>array(
					'en'=>'Overall',
					'th'=>'ภาพรวมโครงการ'
				),
				'link_type'=>'alias'),
			1 => array(
				'id' => '10',
				'alias'=>'smartFarmer',
				'link'=>'#',
				'title'=>array(
					'en'=>'Smart Farmer',
					'th'=>'โครงการ Smart Farmer'
				),
				'link_type'=>'alias'),
			2 => array(
				'id' => '11',
				'alias'=>'chain',
				'link'=>'#',
				'title'=>array(
					'en'=>'Chain',
					'th'=>'การบริหารห่วงโซ่อุปทานยั่งยืน'
				),
				'link_type'=>'alias'),
			3 => array(
				'id' => '12',
				'alias'=>'safeInternet',
				'link'=>'#',
				'title'=>array(
					'en'=>'Safe Internet',
					'th'=>'โครงการ Safe Internet'
				),
				'link_type'=>'alias'),
			4 => array(
				'id' => '13',
				'alias'=>'netArsa',
				'link'=>'#',
				'title'=>array(
					'en'=>'Net ARSA',
					'th'=>'โครงการเน็ตอาสา'
				),
				'link_type'=>'alias')
		),
		'home' => FALSE
	),
	array(
		'id' => '7',
		'alias'=>'aboutme',
		'link'=>'aboutme',
		'title'=>array(
			'en'=>'About me',
			'th'=>'เกี่ยวกับเรา'
		),
		'link_type'=>'dropdown',
		'dropdown'=>array(
			array(
				'id' => '2',
				'alias'=>'service',
				'link'=>'service',
				'title'=>array(
					'en'=>'Service',
					'th'=>'ความยั่งยืนของดีเทค'
				),
				'link_type'=>'alias',
				'dropdown'=>array(),
				'home' => FALSE	
			),
			array(
				'id' => '5',
				'alias'=>'ceotalk',
				'link'=>'ceotalk',
				'title'=>array(
					'en'=>'CEO Talk',
					'th'=>'จากซีอีโอถึงคุณ'
				),
				'link_type'=>'alias',
				'dropdown'=>array()	
			),
			array(
				'id' => '6',
				'alias'=>'sdreport',
				'link'=>'sdreport',
				'title'=>array(
					'en'=>'SD Report',
					'th'=>'รายงานผลการดำเนินงาน'
				),
				'link_type'=>'alias',
				'dropdown'=>array(),
				'home' => FALSE	
			),
		
		),
		'home' => FALSE	
	),
	array(
		'id' => '8',
		'alias'=>'contact',
		'link'=>'contact',
		'title'=>array(
			'en'=>'Contact',
			'th'=>'ติดต่อเรา'
		),
		'link_type'=>'alias',
		'dropdown'=>array(),
		'home' => FALSE
	)
);

// Convert Array to Obj
$menu = json_encode($menu);
$menu = json_decode($menu);
}

function navActiveClass($nav_active, $alias){
	$return = '';
	if($nav_active == $alias){
		$return = ' active';						
	}
	return $return;
}

function navTitleLang($lang, $title){
	$return = $title->th;
	if(isset($lang) && $lang == 'en'){ 
		$return = $title->en; 
	}
	return $return;
}
?>
<nav class="navbar navbar-expand-lg navbar-light bg-transparent" id="main-nav">
	<a class="navbar-brand" href="<?php echo $root; ?>">
		<img class="card-img-top img-fluid" src="<?php echo $root; ?>assets/images/logo.png" srcset="<?php echo $root; ?>assets/images/logo.png 1x, <?php echo $root; ?>assets/images/logo@2x.png 2x" alt="dtac sustainability">
	</a>
	<button class="navbar-toggler" data-target="#my-nav" onclick="expandHamburger(this)" data-toggle="collapse">
		<span class="bar1"></span>
		<span class="bar2"></span>
		<span class="bar3"></span>
	</button>

	<div id="my-nav" class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">
			<?php 
			if(isset($menu) && $menu != ''){
				$menu_html = '';
				$root_link = $root;
				if($demo == true){
					$root_link = $root.'demo2';
				}
				foreach($menu AS $menu_value){
					$nav_active_class	= navActiveClass($nav_active, $menu_value->alias);
					$title				= navTitleLang($nav_active, $menu_value->title);

					switch($menu_value->link_type){
						case 'dropdown':						
								$menu_html .= '<li class="nav-item dropdown">';
								$menu_html .= '<a class="nav-link dropdown-toggle'.$nav_active_class.'" href="'.$root_link.'#" data-toggle="dropdown" title="'.$title.'">'.$title.'</a><div class="dropdown-menu">';
								foreach($menu_value->dropdown AS $menu_dropdown_value){
									$title				= navTitleLang($nav_active, $menu_dropdown_value->title);
									if(isset($lang) && $lang == 'en'){ $title = $menu_dropdown_value->title->en; }
									switch($menu_value->link_type){
										case 'external':
											$menu_html .= '<a class="dropdown-item" href="'.$menu_dropdown_value->link.'" title="'.$title.'" target="_blank">'.$title.'</a>';
											break;

										default :											
											$menu_html .= '<a class="dropdown-item" href="'.$root_link.'/'.$menu_value->alias.'" title="'.$title.'">'.$title.'</a>';
											break;
									}	
								}

							$menu_html .= '</div></li>';
							break;

						case 'external':
							$menu_html .= '<li class="nav-item"><a class="nav-link" href="'.$menu_value->link.'" title="'.$title.'" target="_blank">'.$title.'</a></li>';
							break;

						default :
							$menu_html .= '<li class="nav-item"><a class="nav-link'.$nav_active_class.'" href="'.$root_link.'/'.$menu_value->alias.'" title="'.$title.'">'.$title.'</a></li>';
							break;					
					}
				}
				echo $menu_html;
			} 
			?>	
		</ul>

		<form>
		<ul class="nav navbar-nav navbar-right">
			<li>
				<a<?php if($lang == 'en'){ echo ' class="active"'; } ?> href="#">English</a>
				<br>
				<a<?php if($lang == 'th'){ echo ' class="active"'; } ?> href="#">ไทย</a>
			</li>
			<li id="main-nav-btn " style="margin-top: 10px;"><i class="fas fa-search"></i></li>
		<ul>
		</form>

	</div>
</nav>


