<footer class="container-fluid" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-6 col-lg-8 footer-brand">
				<h1 class="oversize-l"><strong>dtac</strong></h1>
				© 2019. All Rights Reserved.
			</div>
			<div class="col-12 col-sm-6 col-lg-4 footer-nav">
				<nav>
					<div class="row">
						<div class="col-4"><a href="#">Terms</a></div>
						<div class="col-4"><a href="#">Condition</a></div>
						<div class="col-4"><a href="#">Policy</a></div>
					</div>
				</nav>
			</div>
		</div>
	</div>
</footer>

<div id="go-to-top">
	<img class="scrollToTop" src="<?php echo $root; ?>assets/images/Top.png">
</div>
</body>
</html>