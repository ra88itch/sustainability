<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- awesone fonts css-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?php echo $root; ?>assets/starter/css/bootstrap.min.css">

<!-- owl carousel css-->
<link rel="stylesheet" href="<?php echo $root; ?>assets/starter/third-party/owl-carousel/assets/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo $root; ?>assets/starter/third-party/owl-carousel/assets/owl.theme.default.min.css" type="text/css">

<!-- custom CSS -->
<link rel="stylesheet" href="<?php echo $root; ?>assets/starter/css/sustainability.css">

<?php 
if(isset($css)){
	foreach ($css as $css_file) { ?>
<link rel="stylesheet" href="<?php echo $root; ?>assets/starter/css/<?php echo $css_file ?>.css">
<?php }
} ?>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/js/popper.min.js"></script>


<!-- owl carousel js-->
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/third-party/owl-carousel/owl.carousel.min.js"></script>

<?php 
if(isset($js)){
	foreach ($js as $js_file) { ?>
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/<?php echo $js_file; ?>.js"></script>
<?php }
}  ?>
<script type="text/javascript" src="<?php echo $root; ?>assets/starter/js/main.js"></script>


<title><?php echo $title; ?></title>
</head>
<body>