<?php
// http://dtac-dev.codesmash.co.th/sustainability-demo/
?><!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- awesone fonts css-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- owl carousel css-->
    <link rel="stylesheet" href="assets/third-party/owl-carousel/assets/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="assets/third-party/owl-carousel/assets/owl.theme.default.min.css" type="text/css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- custom CSS -->
    <link rel="stylesheet" href="assets/css/sustainability.css">
    <!-- <link rel="stylesheet" href="assets/css/<?php echo $css ?>.css"> -->

    <?php 
    foreach ($css as $data) {
        ?>

        <link rel="stylesheet" href="assets/css/<?php echo $data ?>.css">
        
        <?php
    }

    ?>


    
    <title>dtac Sustainability</title>
    <style>

    </style>
</head>
<body id="body">