<?php
$this->load->view('demo/_head');
$this->load->view('demo/_nav');
?>

<div class="session banner">
	<div class="full-HD">	
		<div class="container-fluid ">

			<div class="row">
				<div class="col-xl-6 col-lg-12 banner-content d-flex align-items-center">

					<div>
						<h1 class="dtac-bold oversize-xxl">นโยบาย<br>ของดีแทค</h1>
						<p>สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร</p>
					</div>


				</div>
				<div class="col-xl-6 col-lg-12 banner-img">
					<div class="card"><img class="" src="assets/images/policy/banner.png" alt=""></div>
				</div>

			</div>

		</div>
	</div>
</div>


<div class="session">
	<div class="container-fluid feature-area">
		<div class="container">
			<div class="row">
				<div class="offset-md-1 col-md-10 text-center">
					<h2 class=""><strong>นโยบายของเรา</strong></h2>
					<p>ข้อความที่เกี่ยวข้องกับนโยบายในการดำเนินงานของ dtac เขียนอธิบายเป็นบทความสั้นๆน่าสนใจ สรุปภาพรวมทั้งหมด หรือจะเกริ่นก่อนเข้าสู่เนื้อหาในแต่ละข้อก็ได้</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="session">
	<div class="toppic">

		<div class="container">

			<div class="show-PC">
				<div class="row menu_zone">


					<div id="bluesky" class="line blue"></div>
					<div id="blue" class="line green"></div>
					<div id="yellow" class="line yellow"></div>

					<div class="col-xl-4 col-lg-4 A fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/policy/icon1.png"  alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon1-w.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การกำกับดูแลและจริยธรรมองค์กร</strong></h2>
								<p class="dtac-light">ดีแทคสร้างวัฒนธรรมองค์กรที่ให้พนักงานรู้สึกมั่นใจที่จะแจ้งเรื่องราวปัญหาเกี่ยวกับจริยธรรมและแจ้งสถานการณ์ที่อาจไม่ถูกต้องตามหลักจริยธรรม </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 B fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/policy/icon2.png" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon2-w.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การกำกับดูแลกิจการ</strong></h2>
								<p class="dtac-light">การกำกับดูแลกิจการที่ดีถือเป็นรากฐานสำคัญของการดำเนินธุรกิจของดีแทค เพราะมีผลอย่างยิ่งต่อความเชื่อมั่นและความน่าเชื่อถือขององค์กร </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 C fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/policy/icon3.png"  alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon3-w.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การรักษาความปลอดภัยของข้อมูลส่วนตัวของลูกค้า</strong></h2>
								<p class="dtac-light">การกำกับดูแลกิจการที่ดีถือเป็นรากฐานสำคัญของการดำเนินธุรกิจของดีแทค เพราะมีผลอย่างยิ่งต่อความเชื่อมั่นและความน่าเชื่อถือขององค์กร </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 A fix_col400">
						<div class="box" atl="buttom">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/policy/icon4.png"  alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon4-w.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การดูแลพนักงาน</strong></h2>
								<p class="dtac-light">ที่ดีแทค พนักงานถือเป็นองค์ประกอบที่มีค่าสูงที่สุดในการขับเคลื่อนองค์กรไปสู่การเติบโตอย่างยั่งยืน ในช่วงแห่งการเปลี่ยนผ่านทางดิจิทัล </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 B fix_col400">
						<div class="box" atl="buttom">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/policy/icon5.png" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon5-w.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การดำเนินงานด้านสิ่งแวดล้อม</strong></h2>
								<p class="dtac-light">ดีแทคมีเป้าหมายการเป็นองค์กรที่ดำเนินธุรกิจอย่างมีความรับผิดชอบต่อสิ่งแวดล้อม และได้กำหนดประเด็นการดำเนินงานด้านสิ่งแวดล้อมเป็นส่วนหนึ่งของนโยบายด้านความยั่งยืน 	</p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

				</div>
			</div>		

			<div class="show-M">
				<div class="owl-carousel owl-theme owl-loaded">
					<div class="owl-stage-outer">
						<div class="owl-stage">

							<div class="owl-item">
								<div class="box-sm box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/policy/icon1.png"  alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon1-w.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การกำกับดูแลและจริยธรรมองค์กร</strong></h2>
										<p class="dtac-light">ดีแทคสร้างวัฒนธรรมองค์กรที่ให้พนักงานรู้สึกมั่นใจที่จะแจ้งเรื่องราวปัญหาเกี่ยวกับจริยธรรมและแจ้งสถานการณ์ที่อาจไม่ถูกต้องตามหลักจริยธรรม </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/policy/icon2.png" alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon2-w.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การกำกับดูแลกิจการ</strong></h2>
										<p class="dtac-light">การกำกับดูแลกิจการที่ดีถือเป็นรากฐานสำคัญของการดำเนินธุรกิจของดีแทค เพราะมีผลอย่างยิ่งต่อความเชื่อมั่นและความน่าเชื่อถือขององค์กร </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/policy/icon3.png"  alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon3-w.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การรักษาความปลอดภัยของข้อมูลส่วนตัวของลูกค้า</strong></h2>
										<p class="dtac-light">การกำกับดูแลกิจการที่ดีถือเป็นรากฐานสำคัญของการดำเนินธุรกิจของดีแทค เพราะมีผลอย่างยิ่งต่อความเชื่อมั่นและความน่าเชื่อถือขององค์กร </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm box-sm d-flex justify-content-center" atl="buttom">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/policy/icon4.png"  alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon4-w.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การดูแลพนักงาน</strong></h2>
										<p class="dtac-light">ที่ดีแทค พนักงานถือเป็นองค์ประกอบที่มีค่าสูงที่สุดในการขับเคลื่อนองค์กรไปสู่การเติบโตอย่างยั่งยืน ในช่วงแห่งการเปลี่ยนผ่านทางดิจิทัล </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm box-sm d-flex justify-content-center" atl="buttom">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/policy/icon5.png" alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/policy/icon5-w.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การดำเนินงานด้านสิ่งแวดล้อม</strong></h2>
										<p class="dtac-light">ดีแทคมีเป้าหมายการเป็นองค์กรที่ดำเนินธุรกิจอย่างมีความรับผิดชอบต่อสิ่งแวดล้อม และได้กำหนดประเด็นการดำเนินงานด้านสิ่งแวดล้อมเป็นส่วนหนึ่งของนโยบายด้านความยั่งยืน 	</p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>



		</div>

	</div>
</div>

<div class="container-fluid ">
	<div class="session project">
		<div class="full-HD">	

			<div class="row">
				<div class="project-content d-flex align-items-center">

					<div class="content">
						<h2 class="dtac-bold">
							<strong>
								เทสโก้ โลตัส จับมือ ดีแทค 
								เปิดตัวโครงการเก็บรวบรวมโทรศัพท์มือถือและอุปกรณ์
								ที่ไม่ใช้แล้ว ก่อนนำไปรีไซเคิล
								อย่างถูกวิธี 
							</strong>
						</h2>
						<p class="dtac-light">
							เทสโก้ โลตัส  จับมือ ดีแทค เปิดตัวโครงการเก็บรวบรวมโทรศัพท์มือถือและอุปกรณ์ที่ไม่ใช้แล้ว ก่อนนำไปรีไซเคิลอย่างถูกวิธี ลดปัญหาสิ่งแวดล้อม ตั้งเป้าจุดพลุโครงการรณรงค์ประชาชนยุค 4.0 ตื่นตัว ผ่านเฟสแรกในร้านค้าเทสโก้ โลตัส กลางกรุง หวังลดปริมาณขยะอันตรายในไทย ขับเคลื่อนวาระแห่งชาติ
						</p>
						<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					</div>


				</div>
				<div class="project_sl d-flex align-items-center">

					<div style="" class="owl-carousel owl-theme owl-loaded ">
						<div class="owl-stage-outer">
							<div class="owl-stage">
								<div class="owl-item">
									<img style="" class="card-img-top radius100" src="assets/images/policy/sl_1.png" alt="Card image cap">
								</div>
								<div class="owl-item">
									<img style="" class="card-img-top radius100" src="assets/images/policy/sl_2.png" alt="Card image cap">
								</div>
								<div class="owl-item">
									<img style="" class="card-img-top radius100" src="assets/images/policy/sl_3.png" alt="Card image cap">
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="session news">
	<div class="full-HD">
		<div class="container-fluid ">
			<div class="row news-content">
				<h1 class="dtac-bold oversize-l">ข่าวและกิจกรรมของเรา</h1>
				<div class="owl-carousel owl-theme owl-loaded">
					<div class="owl-stage-outer">
						<div class="owl-stage">
							<?php 
							for ($i=1; $i < 7; $i++) { 
								?>
								<div class="owl-item">
									<a target="_bank" href="https://community.dtac.co.th/">
										<div class="card col">
											<img class="card-img-top"  src="assets/images/main/sl_<?php echo $i ?>.png" alt="Card image cap">
											<div class="card-body">
												<p class="card-text">dtac ฟักฝันเฟส จัดงานเทศกาลแนะแนว
													อาชีพและสร้างแรงบันดาลใจสำหรับเด็ก
												ม.1-6 ที่ใหญ่ที่สุดในประเทศเพื่อให้...</p>
											</div>
										</div>
									</a>
								</div>
								<?php
							}
							?>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>



<?php
$this->load->view('demo/_totop');
$this->load->view('demo/_footer');
$this->load->view('demo/_script');
$this->load->view('demo/owl');
$this->load->view('demo/topic-box');
$this->load->view('demo/scrollTop');
?>