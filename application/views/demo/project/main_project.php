<?php
$this->load->view( 'demo/_head' );
$this->load->view( 'demo/_nav' );
?>

<div class="session banner">
	<div class="full-HD">	


		<div class="container-fluid ">

			<div class="row">
				<div class="col-xl-5 col-md-12 banner-content d-flex align-items-center">

					<div>
						<h1 class="dtac-bold oversize-xxl">สิ่งที่เราทำ<br>เพื่อความยั่งยืน</h1>
						<p>สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร</p>
						<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					</div>


				</div>
				<div id="lottie_col" class="col-xl-7 col-md-12 banner-img">
					<div class="lottie_baner">
						<div id="baner_layer_3"></div>
						<div id="baner_layer_2"></div>
						<div id="baner_layer_1"></div>
					</div>
				</div>

			</div>

		</div>


	</div>
</div>


<div class="session">
	<div class="container-fluid feature-area">
		<div class="container">
			<div class="row">
				<div class="offset-md-1 col-md-10 text-center">
					<h2 class=""><strong>โครงการต่างๆของเรา</strong></h2>
					<p>สร้างความยั่งยืนให้แก่ประเทศชาติ ดีแทคได้ยึดถือหลักการ ‘ทำในสิ่งที่ถูกต้อง’ หรือ Do the Right Thing ควบคู่ไปกับการ ‘สร้างคุณค่าร่วม’ หรือ Creating Shared Value (CSV) ให้แก่สังคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค ‘สร้างไทยให้แกร่ง’ และการมุ่งสู่เป้าหมายการพัฒนาอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="session">
	<div class="container">
		<div class="row d-flex justify-content-center">

			<div class="box-project  card text-center" style="width: 18rem;" onclick="window.location.href='#smart-farmer'">
				<div class="d-none  d-xl-block">
					<img class="project-img" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
					<div class="card-body">
						<p class="card-text">โครงการ Smart Farmer</p>
					</div>
				</div>
				<div class="d-xl-none" style="display:inline-flex; height: 100%">
					<div style="width: 40%; height: 100%;">
						<img class="project-img" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
					</div>
					<div style="width: 60%; height: 100%; text-align: left;">
						<div style="height: 150px;margin: 0px;display: table-cell;vertical-align: middle;">
							โครงการ Smart Farmer
						</div>
					</div>
				</div>
			</div>

			<div class="box-project  card text-center" style="width: 18rem;" onclick="window.location.href='#chain'">
				<div class="d-none d-xl-block">
					<img class="project-img" src="assets/images/ethics.png" srcset="assets/images/ethics.png 1x, assets/images/ethics@2x.png 2x" alt="">
					<div class="card-body">
						<p class="card-text">การบริหารห่วงโซ่อุปทานอย่างยั่งยืน</p>
					</div>
				</div>
				<div class="d-xl-none" style="display:inline-flex; height: 100%">
					<div style="width: 40%; height: 100%;">
						<img class="project-img" src="assets/images/ethics.png" srcset="assets/images/ethics.png 1x, assets/images/ethics@2x.png 2x" alt="">
					</div>
					<div style="width: 60%; height: 100%; text-align: left;">
						<div style="height: 150px;margin: 0px;display: table-cell;vertical-align: middle;">
							การบริหารห่วงโซ่อุปทานอย่างยั่งยืน
						</div>
					</div>
				</div>
			</div>

			<div class="box-project  card text-center" style="width: 18rem;" onclick="window.location.href='#safe-internet'">
				<div class="d-none d-xl-block">
					<img class="project-img" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
					<div class="card-body">
						<p class="card-text">โครงการ Safe Internet</p>
					</div>
				</div>
				<div class="d-xl-none" style="display:inline-flex; height: 100%">
					<div style="width: 40%; height: 100%;">
						<img class="project-img" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
					</div>
					<div style="width: 60%; height: 100%; text-align: left;">
						<div style="height: 150px;margin: 0px;display: table-cell;vertical-align: middle;">
							โครงการ Safe Internet
						</div>
					</div>
				</div>
			</div>

			<div class="box-project  card text-center" style="width: 18rem;" onclick="window.location.href='#net-arsa'">
				<div class="d-none d-xl-block">
					<img class="project-img" src="assets/images/data-secure.png" srcset="assets/images/data-secure.png 1x, assets/images/data-secure@2x.png 2x" alt="">
					<div class="card-body">
						<p class="card-text">โครงการเน็ตอาสา</p>
					</div>
				</div>
				<div class="d-xl-none" style="display:inline-flex; height: 100%">
					<div style="width: 40%; height: 150px; display: table-cell;vertical-align: middle;">
						<img class="project-img" src="assets/images/data-secure.png" srcset="assets/images/data-secure.png 1x, assets/images/data-secure@2x.png 2x" alt="">
					</div>
					<div style="width: 60%; height: 100%; text-align: left;">
						<div style="height: 150px;margin: 0px;display: table-cell;vertical-align: middle;">
							โครงการเน็ตอาสา
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="session project_smartfarmer" id="smart-farmer">
	<div class="box-session">
		<div class="container">
			<div class="row">
				<div class="col box_left d-none d-xl-block">
					<h2>2557 - ปัจุบัน</h2>
					<div class="balloon_text_left ">
						<h2>เกษตรกร </br>
							รายได้เพิ่มขึ้น </br>
							<font style="color: black !important;">25% </font></br>
						</h2>
					</div>
					<div style="position: absolute"></div>
				</div>
				<div class="space">
					<div class="dot"></div>
					<div class="line"></div>
				</div>
				<div class="col box_right">
					<h2><strong>โครงการ Smart Farmer</strong></h2>
					<p>ดีแทคได้ริเริ่มโครงการ Smart Farmer ตั้งแต่ปี 2551 โดยมีเป้าหมายสูงสุดคือ เพื่อพัฒนาอุตสาหกรรมการเกษตรเพื่อให้เป็นฐานรากที่สำคัญในการพัฒนาเศรษฐกิจและสังคมไทย ด้วยการใช้ประโยชน์จากเทคโนโลยีสารสนเทศเป็นเครื่องมือหลัก ลดความเหลื่อมล้ำในการเข้าถึงข้อมูล องค์ความรู้ และรับบริการจากภาครัฐ ให้สามารถเพิ่มรายได้และคุณภาพชีวิตแก่เกษตรกร ซึ่งคิดเป็น 1 ใน 3 ของจำนวนประชากรทั้งหมดในประเทศ</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>

					<div class="balloon_text_right d-none d-xl-block">
						<h2>อบรมเกษตรกร</br>
							<font style="color: black !important;">20,000</font> ราย</br>
							ทั่วประเทศ</br>
							<font class="dtac-bold">25% </font></br>
						</h2>
					</div>
				</div>
			</div>
		</div>
		<div id="lottie_smart" class="full-HD">
			<div class="row">
				<div class="col box_all">
					<div id="circle_2"> <img width="100%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/smartfarmer/circle_2.png" alt=""> </div>
					<div id="circle_1"> <img width="100%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/smartfarmer/circle_1.png" alt=""> </div>
					<div id="smartlayer3"></div>
					<div id="smartlayer2"></div>
					<div id="smartlayer1"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="session project_net" id="net-arsa">
	<div class="box-session">
		<div class="container">
			<div class="row">
				<div class="col box_left d-none d-xl-block">
					<h2><strong>โครงการเน็ตอาสา</strong></h2>
					<p>ดีแทคได้ริเริ่มโครงการเน็ตอาสาตั้งแต่ปี 2557 มีเป้าหมายหลักคือ การเป็นกลไกสำคัญในการสร้างความร่วมมือระหว่างภาครัฐ หน่วยงานภาคประชาสังคม และดีแทค
เพื่อนำไปสู่การสร้างผลลัพธ์สำคัญในการขับเคลื่อนการพัฒนาเศรษกิจและสังคม
ที่ครอบคลุมตั้งแต่ระดับรากหญ้าจนถึงระดับประเทศ

</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>

					
				</div>
				<div class="space">
					<div class="dot"></div>
					<div class="line"></div>
				</div>
				<div class="col box_right d-none d-xl-block">
					<h2>2557 - ปัจุบัน</h2>
					<div class="bolloon_netarsa" style="margin-top: 10%">
						<img width="80%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/NetArsa/Group 3253.png" alt="">
					</div>
				</div>
				<div class="col box_right d-xl-none">
					<h2><strong>โครงการเน็ตอาสา</strong></h2>
					<p>ดีแทคได้ริเริ่มโครงการเน็ตอาสาตั้งแต่ปี 2557 มีเป้าหมายหลักคือ การเป็นกลไกสำคัญในการสร้างความร่วมมือระหว่างภาครัฐ หน่วยงานภาคประชาสังคม และดีแทค
เพื่อนำไปสู่การสร้างผลลัพธ์สำคัญในการขับเคลื่อนการพัฒนาเศรษกิจและสังคม
ที่ครอบคลุมตั้งแต่ระดับรากหญ้าจนถึงระดับประเทศ

</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					<div class="bolloon_netarsa" style="margin-top: 10%">
						<img width="80%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/NetArsa/Group 3253.png" alt="">
					</div>
					
				</div>
			</div>
		</div>
		<div id="lottie_smart" class="full-HD">
			<div class="row">
				<div class="col box_all">
					<div class="netarsa_lottie">
						<div id="net_lottie"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="session project_safe" id="safe-internet">
	<div class="box-session">
		<div class="container">
			<div class="row">
				<div class="col box_left d-none d-xl-block">
					<h2>2557 - ปัจุบัน</h2>
				</div>
				<div class="space">
					<div class="dot"></div>
					<div class="line"></div>
				</div>
				<div class="col box_right">
					<h2><strong>โครงการ Safe Internet</strong></h2>
					<p>ดีแทคได้วางตำแหน่งด้านการดำเนินธุรกิจอย่างเป็นมิตรกับเด็กและเยาวชน (Child-friendly Business) สอดคล้องกับแนวทางการดำเนินธุรกิจอย่างรับผิดชอบ ซึ่งถือเป็นหัวใจสำคัญของการทำงานของดีแทค โดยถูกกำหนดอยู่ในนโยบายด้านความรับผิดชอบต่อสังคมของดีแทค ในเรื่องการส่งเสริมให้ประชาชนทุกกลุ่มได้ใช้ประโยชน์จากเทคโนโลยีสารสนเทศ และการสร้างสภาพแวดล้อมที่ปลอดภัยแก่ผู้ใช้งานออนไลน์ (Enable and Safe) โดยบริษัทได้เล็งเห็นแล้วว่า การใช้การสื่อสารทางออนไลน์หรือการใช้ข้อมูลอย่างไม่เหมาะสม ซึ่งสามารถนำไปสู่การสูญเสียในรูปแบบใดรูปแบบหนึ่งได้ โดยเฉพาะในกลุ่มเด็กและเยาวชนที่ยังมีความรู้และทักษะในการปกป้องตนเองในโลกออนไลน์อย่างจำกัด ในขณะที่การเข้าถึงอินเทอร์เนตในประชากรกลุ่มนี้กลับเพิ่มขึ้นอย่างต่อเนื่อง ในฐานะผู้ให้บริการด้านอินเทอร์เนตและการสื่อสาร จึงเป็นความรับผิดชอบของดีแทคที่จะต้องสร้างภูมิคุ้มกันในการท่องโลกออนไลน์ให้แก่เด็กและเยาวชน จึงได้ดำเนินโครงการ Safe Internet อย่างต่อเนื่องมาตั้งแต่ปี 2557
</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>

				</div>
			</div>
		</div>
		<div id="lottie_smart" class="full-HD">
			<div class="row">
				<div class="col box_all">
					<div class="safe_lottie">
						<div class="safe_layer_3">
							<img width="60%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/safe/Group 3250.png" alt="">
						</div>
						<div class="safe_layer_2">
							<div id="safe_lottie"></div>
						</div>
						<div class="safe_layer_1">
							<img width="60%" height="auto" src="<?php echo base_url(); ?>/assets/images/project/safe/Group 3252.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="session project_chain" id="chain">
	<div class="box-session">
		<div class="container">
			<div class="row">
				<div class="col box_left d-none d-xl-block">
					<h2><strong>การบริหารจัดการห่วงโซ่อุปทานอย่างยั่งยืน</strong></h2>
					<p>ดีแทคให้ความสำคัญต่อผู้จัดหาสินค้าและบริการทุกรายและถือว่าเป็นหนึ่งในกลุ่มผู้มีส่วนได้ส่วนเสียที่สำคัญต่อกระบวนการดำเนินธุรกิจของบริษัท สถานการณ์หรือการดำเนินงานใดๆ ที่ถือเป็นความเสี่ยงต่อการละเมิดกฎหมายไม่เพียงแต่ส่งผลเสียต่อการดำเนินธุรกิจและชื่อเสียงของบริษัทคู่ค้าและดีแทคเท่านั้น แต่อาจส่งผลกระทบต่อการรักษาสิทธิมนุษยชนในระดับประเทศและการพัฒนาในอุตสาหกรรมโทรคมนาคมด้วยเช่นกัน

</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>

					
				</div>
				<div class="space">
					<div class="dot"></div>
					<div class="line"></div>
				</div>
				<div class="col box_right d-none d-xl-block">
					<h2>2561 - ปัจุบัน</h2>
				</div>
				<div class="col box_right d-xl-none">
					<h2><strong>การบริหารจัดการห่วงโซ่อุปทานอย่างยั่งยืน</strong></h2>
					<p>ดีแทคให้ความสำคัญต่อผู้จัดหาสินค้าและบริการทุกรายและถือว่าเป็นหนึ่งในกลุ่มผู้มีส่วนได้ส่วนเสียที่สำคัญต่อกระบวนการดำเนินธุรกิจของบริษัท สถานการณ์หรือการดำเนินงานใดๆ ที่ถือเป็นความเสี่ยงต่อการละเมิดกฎหมายไม่เพียงแต่ส่งผลเสียต่อการดำเนินธุรกิจและชื่อเสียงของบริษัทคู่ค้าและดีแทคเท่านั้น แต่อาจส่งผลกระทบต่อการรักษาสิทธิมนุษยชนในระดับประเทศและการพัฒนาในอุตสาหกรรมโทรคมนาคมด้วยเช่นกัน

</p>
					<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>

					
				</div>
			</div>
		</div>
		<div id="lottie_smart">
			<div class="row">
				<div class="col box_all">
					<div class="chain_lottie">
						<div id="chain_lottie" style="width: 90%; margin: 0 auto">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<!--<div class="session">
	<div class="box-session">
		<div class="container-fluid">
			<div class="link">
				<img class="scrollToTop" width="auto" onclick="scrolltop()" height="auto" src="assets/images/Top.png" alt="">
			</div>
		</div>
	</div>
</div>-->


<?php
$this->load->view( 'demo/_footer' );
$this->load->view( 'demo/_script' );
$this->load->view( 'demo/scrollTop' );
$this->load->view( 'demo/project/script' );
?>