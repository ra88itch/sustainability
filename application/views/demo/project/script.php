<script src="<?php echo base_url();?>assets/js/lottie.js"></script>
<script>
	
	lottie.loadAnimation( {
		container: document.getElementById( 'baner_layer_1' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/banner/json/layer_1.json' // the path to the animation json
	} );
	lottie.loadAnimation( {
		container: document.getElementById( 'baner_layer_2' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/banner/json/layer_2.json' // the path to the animation json
	} );
	lottie.loadAnimation( {
		container: document.getElementById( 'baner_layer_3' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/banner/json/layer_3.json' // the path to the animation json
	} );
	
	lottie.loadAnimation( {
		container: document.getElementById( 'net_lottie' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/NetArsa/json/net.json' // the path to the animation json
	} );
	
	
	lottie.loadAnimation( {
		container: document.getElementById( 'safe_lottie' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/safe/json/safe.json' // the path to the animation json
	} );
	
	
	lottie.loadAnimation( {
		container: document.getElementById( 'chain_lottie' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/chain/json/chain.json' // the path to the animation json
	} );
	
	
	lottie.loadAnimation( {
		container: document.getElementById( 'smartlayer2' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/smartfarmer/json/human.json' // the path to the animation json
	} );
	lottie.loadAnimation( {
		container: document.getElementById( 'smartlayer3' ), // the dom element that will contain the animation
		renderer: 'svg',
		loop: true,
		autoplay: true,
		path: '<?php echo base_url();?>assets/images/project/smartfarmer/json/line.json' // the path to the animation json
	} );
	
	 var StartAnimation = ($(".project_smartfarmer").offset().top) - 100;
	 var StartAnimation2 = ($(".project_smartfarmer").offset().top) - 100;
	 var ContentHeight = $(".project_smartfarmer").outerHeight(true);
	
	console.log('StartAnimation:'+ StartAnimation);
	
	
	
    var anim;
    var elem = document.getElementById('smartlayer1')
    var animData = {
        container: elem,
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false,
            preserveAspectRatio: 'xMidYMid slice'
        },
       path: '<?php echo base_url();?>assets/images/project/smartfarmer/json/bg.json'
    };
    anim = lottie.loadAnimation(animData);

    $(window).scroll(function() {
		
		if($(window).scrollTop() > StartAnimation)
		{
			

			// calculate the percentage the user has scrolled down the page
		
			var scrollPercent2 = 100 * ($(window).scrollTop() - StartAnimation) / (400);
			var scrollPercent3 = 100 * ($(window).scrollTop() - StartAnimation) / (400);

		   // console.log(anim.currentRawFrame);

			scrollPercentRounded2 = Math.round(scrollPercent2);
			scrollPercentRounded3 = Math.round(scrollPercent3);

			//console.log("scroll:"+ scrollPercentRounded );
			if((scrollPercentRounded2 / 100) * 400 <= 380){
				$('.balloon_text_left').css('left', (scrollPercentRounded2 / 100) * 436 +'px');
				$('.balloon_text_right').css('right', (scrollPercentRounded3 / 100) * 436 +'px');
			}
			
			
		}
		if($(window).scrollTop() > StartAnimation2)
		{
			

			// calculate the percentage the user has scrolled down the page
			var scrollPercent = 100 * ($(window).scrollTop() - StartAnimation2) / (ContentHeight);

		   // console.log(anim.currentRawFrame);

			scrollPercentRounded = Math.round(scrollPercent);
		
			anim.goToAndStop((scrollPercentRounded / 100) * 104, 'ture')
			
		}
    });





</script>