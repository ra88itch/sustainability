<?php
$this->load->view('demo/_head');
$this->load->view('demo/_nav');
?>


<div class="session banner">
	<div class="full-HD">	


		<div class="container-fluid ">

			<div class="row">
				<div class="col-xl-5 col-lg-12 banner-content d-flex align-items-center">

					<div>
						<h1 class="dtac-bold oversize-xxl">ดีแทค<br>กับความยั่งยืน</h1>
						<p>สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร</p>
						<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					</div>


				</div>
				<div id="lottie_col" class="col-xl-7 col-md-12 banner-img">
					<div class="lottie_baner">
						<div id="baner_layer_1"></div>
					</div>
				</div>

			</div>

		</div>


	</div>
</div>


<div class="session">
	<div class="container-fluid feature-area">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 text-center">
					<h2 class=""><strong>กลยุทธ์การพัฒนาอย่างยั่งยืนของเรา</strong></h2>
					<p>สร้างความยั่งยืนให้แก่ประเทศชาติ ดีแทคได้ยึดถือหลักการ 'ทำในสิ่งที่ถูกต้อง' หรือ Do the Right Thing ควบคู่ไปกับการ 'สร้างคุณค่ารวม' หรือ Creating Shared Value (CSV) ให้แก่สั่งคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค 'สร้างไทยให้แกร่ง' และมุ่งสู่เป้าหมายอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="session">
	<div class="toppic">

		<div class="container">

			<div class="show-PC">
				<div class="row menu_zone">


					<div id="bluesky" class="line blue"></div>
					<div id="blue" class="line green"></div>
					<div id="yellow" class="line yellow"></div>

					<div class="col-xl-4 col-lg-4 A fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/plan_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>กลยุทธ์ของเรา</strong></h2>
								<p class="dtac-light">ดีแทคได้จำทำนโยบายการดำเนินงานด้านความยั่งยืน โดยใช้มาตรฐานระดับสากล อาทิ UN Global Compact, UN Universal Declaration of Human Rights, ILO Core Conventions เป็นต้น</p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 B fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/data-secure.png" srcset="assets/images/data-secure.png 1x, assets/images/data-secure@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/data-secure_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การรักษาความปลอดภัยของข้อมูลลูกค้า</strong></h2>
								<p class="dtac-light">ดีแทคเป็นองค์กรที่คำนึงถึงข้อมูลความเป็นส่วนตัวของลูกค้าในการดำเนินธุรกิจอย่างเคร่งครัด</p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 C fix_col400">
						<div class="box"  atl="top">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/environment.png" srcset="assets/images/environment.png 1x, assets/images/environment@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/environment_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การดำเนินงานด้านสิ่งแวดล้อม</strong></h2>
								<p class="dtac-light">ดีแทคมีเป้าหมายการเป็นองค์กรที่ดำเนินธุรกิจอย่างมีความรับผิดชอบต่อสิ่งแวดล้อม และได้กำหนดประเด็นการดำเนินงานด้านสิ่งแวดล้อมเป็นส่วนหนึ่งของนโยบายด้านความยั่งยืน</p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 A fix_col400">
						<div class="box" atl="buttom">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/ethics.png" srcset="assets/images/ethics.png 1x, assets/images/ethics@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/ethics_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>การกำกับดูแลและจริยธรรมองค์กร</strong></h2>
								<p class="dtac-light">ดีแทคได้ริเริ่มโครงการ Smart Farmer ตั้งแต่ปี 2551 โดย มีเป้าหมายสูงสุดคือ เพื่อพัฒนาอุตสาหกรรมการเกษตรเพื่อให้เป็นฐานรากที่สำคัญในการ พัฒนาเศรษฐกิจและสังคมไทย </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 B fix_col400">
						<div class="box" atl="buttom">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/smart-farmer.png" srcset="assets/images/smart-farmer.png 1x, assets/images/smart-farmer@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/smart-farmer_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>โครงการ Smart Farmer</strong></h2>
								<p class="dtac-light">ดีแทคได้ริเริ่มโครงการ Smart Farmer ตั้งแต่ปี 2551 โดยมีเป้าหมายสูงสุดคือ เพื่อพัฒนาอุตสาหกรรมการเกษตรเพื่อให้เป็นฐานรากที่สำคัญในการพัฒนาเศรษฐกิจและสังคมไทย </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

					<div class="col-xl-4 col-lg-4 C fix_col400">
						<div class="box" atl="buttom">
							<div class="icon">
								<img class="img-fluid show1" src="assets/images/save-internet.png" srcset="assets/images/save-internet.png 1x, assets/images/save-internet@2x.png 2x" alt="">
								<img class="img-fluid show2" style="display: none" src="assets/images/save-internet_light.png" alt="">
							</div>
							<div class="text">
								<h2 class="head-box"><strong>โครงการ Save Internet</strong></h2>
								<p class="dtac-light">ดีแทคได้วางตำแหน่งด้านการดำเนินธุรกิจอย่างเป็นมิตรกับเด็กและเยาวชน (Child-friendly Business) สอดคล้องกับแนวทางการดำเนินธุรกิจอย่างรับผิดชอบ </p>
								<p class="">อ่านเพิ่ม</p>
							</div>
						</div>
					</div>

				</div>
			</div>		

			<div class="show-M">
				<div class="owl-carousel owl-theme owl-loaded">
					<div class="owl-stage-outer">
						<div class="owl-stage">

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/plan.png" srcset="assets/images/plan.png 1x, assets/images/plan@2x.png 2x" alt="">
										<img class="img-fluid show2" style="display: none" src="assets/images/plan_light.png" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>กลยุทธ์ของเรา</strong></h2>
										<p class="dtac-light">ดีแทคได้จำทำนโยบายการดำเนินงานด้านความยั่งยืน โดยใช้มาตรฐานระดับสากล อาทิ UN Global Compact, UN Universal Declaration of Human Rights, ILO Core Conventions เป็นต้น</p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/data-secure.png" srcset="assets/images/data-secure.png 1x, assets/images/data-secure@2x.png 2x" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การรักษาความปลอดภัยของข้อมูลลูกค้า</strong></h2>
										<p class="dtac-light">ดีแทคเป็นองค์กรที่คำนึงถึงข้อมูลความเป็นส่วนตัวของลูกค้าในการดำเนินธุรกิจอย่างเคร่งครัด</p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center"  atl="top">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/environment.png" srcset="assets/images/environment.png 1x, assets/images/environment@2x.png 2x" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การดำเนินงานด้านสิ่งแวดล้อม</strong></h2>
										<p class="dtac-light">ดีแทคมีเป้าหมายการเป็นองค์กรที่ดำเนินธุรกิจอย่างมีความรับผิดชอบต่อสิ่งแวดล้อม และได้กำหนดประเด็นการดำเนินงานด้านสิ่งแวดล้อมเป็นส่วนหนึ่งของนโยบายด้านความยั่งยืน</p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center" atl="buttom">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/ethics.png" srcset="assets/images/ethics.png 1x, assets/images/ethics@2x.png 2x" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>การกำกับดูแลและจริยธรรมองค์กร</strong></h2>
										<p class="dtac-light">ดีแทคได้ริเริ่มโครงการ Smart Farmer ตั้งแต่ปี 2551 โดย มีเป้าหมายสูงสุดคือ เพื่อพัฒนาอุตสาหกรรมการเกษตรเพื่อให้เป็นฐานรากที่สำคัญในการ พัฒนาเศรษฐกิจและสังคมไทย </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center" atl="buttom">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/smart-farmer.png" srcset="assets/images/smart-farmer.png 1x, assets/images/smart-farmer@2x.png 2x" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>โครงการ Smart Farmer</strong></h2>
										<p class="dtac-light">ดีแทคได้ริเริ่มโครงการ Smart Farmer ตั้งแต่ปี 2551 โดยมีเป้าหมายสูงสุดคือ เพื่อพัฒนาอุตสาหกรรมการเกษตรเพื่อให้เป็นฐานรากที่สำคัญในการพัฒนาเศรษฐกิจและสังคมไทย </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

							<div class="owl-item">
								<div class="box-sm d-flex justify-content-center" atl="buttom">
									<div class="icon">
										<img class="img-fluid show1" src="assets/images/save-internet.png" srcset="assets/images/save-internet.png 1x, assets/images/save-internet@2x.png 2x" alt="">
									</div>
									<div class="text">
										<h2 class="head-box"><strong>โครงการ Save Internet</strong></h2>
										<p class="dtac-light">ดีแทคได้วางตำแหน่งด้านการดำเนินธุรกิจอย่างเป็นมิตรกับเด็กและเยาวชน (Child-friendly Business) สอดคล้องกับแนวทางการดำเนินธุรกิจอย่างรับผิดชอบ </p>
										<p class="">อ่านเพิ่ม</p>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>



		</div>

	</div>
</div>

<div class="container-fluid ">
	<div class="session youtube">
		<div class="full-HD">	
			<div class="row">
				<div class="youtube-video-content d-flex align-items-center">

					<div class="content">
						<h1 class="dtac-bold oversize-l">ก้าวต่อไปอย่างยั่งยืน<br>กับ อเล็กซานดรา ไรช์ ซีอีโอ<br>คนใหม่ของดีแทค</h1>

						<p>ในปี 2561 ที่ผ่านมา ถือได้ว่าเป็นอีกปีแห่งการเปลี่ยนผ่านของอุตสาหกรรมโทรคมนาคม โดยถือเป็นปีที่สิ้นสุดระบบสัมปทานและก้าวสู่ระบบใบอนุญาตอย่างเต็มรูปแบบ โดยสัญญาสัมปทานระหว่างดีแทคและบริษัท กสท. โทรคมนาคม จำกัด (มหาชน) นับเป็นสัญญาสัมปทานฉบับสุดท้ายของอุตสาหกรรม ซึ่งจะก่อให้เกิดประโยชน์ทางเศรษฐกิจอีกนานัปการ</p>


						<a href=""><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					</div>


				</div>
				<div class="youtube-video-list">

					<iframe id="video" width="100%" height="450px" src="https://www.youtube.com/embed/nfQrjutRptc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<div class="show-PC ">

						<div class="row thumbnails-video-list">
							<div class="col"><div onclick="change_video('nfQrjutRptc')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/nfQrjutRptc/0.jpg');" class="img-fluid thumbnails-video"></div></div>
							<div class="col"><div onclick="change_video('L1AEo8U9FWo')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/L1AEo8U9FWo/0.jpg');" class="img-fluid thumbnails-video"></div></div>
							<div class="col"><div onclick="change_video('IuhD4jKvs7M')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/IuhD4jKvs7M/0.jpg');" class="img-fluid thumbnails-video"></div></div>
						</div>

					</div>
					<!-- <div class="col show-M">
						<div style="" class="owl-carousel owl-theme owl-loaded ">
							<div class="owl-stage-outer">
								<div class="owl-stage">
									<div class="owl-item col">
										<div onclick="change_video('nfQrjutRptc')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/nfQrjutRptc/0.jpg');" class="img-fluid thumbnails-video"></div>
									</div>
									<div class="owl-item col">
										<div onclick="change_video('L1AEo8U9FWo')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/L1AEo8U9FWo/0.jpg');" class="img-fluid thumbnails-video"></div>
									</div>
									<div class="owl-item col">
										<div onclick="change_video('IuhD4jKvs7M')" style="cursor: pointer; background-image: url('http://img.youtube.com/vi/IuhD4jKvs7M/0.jpg');" class="img-fluid thumbnails-video"></div>
									</div>
								</div>
					
							</div>
					
						</div>
					</div> -->

				</div>

			</div>
		</div>
	</div>
</div>

<div class="session">
	<div class="container-fluid ">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="dtac-bold oversize-l">โครงการ Smart Farmer</h1>
					<p>ในปี 2561 ดีแทคยังคงให้ความสำคัญกับการพัฒนาเทคโนโลยีดิจิทัลเพื่อตอบสนองต่อปัญหาและความต้องการของเกษตรกรรายย่อยควบคู่ไปกับการสร้างทักษะที่จำเป็นในการเป็นผู้ประกอบการในยุคดิจิทัล โดยโครงการที่เกิดขึ้นภายใต้โครงการ Smart Farmer ดังนี้</p>

				</div>

			</div>
		</div>
	</div>
</div>


<div class="container-fluid ">
	<div class="session project">
		<div class="full-HD">	

			<div class="row">
				<div class="project-content d-flex align-items-center">

					<div class="content">
						<img class="card-img-top img-head img-responsive" src="assets/images/main/LOGO_FARMMANYUM.png"  alt="Card image cap">
						<p class="card-text">ต่อยอดจากความร่วมมือทางการเกษตร หลังจากความร่วมมือระหว่าง ดีแทค และกรมส่งเสริมการเกษตร 
						ในการพัฒนาความรู้ความเข้าใจด้านเกษตรครบวงจร ตั้งแต่การผลิต แปรรูปและการตลาดออนไลน์</p>
						<a href="<?php echo base_url('project'); ?>"><button class="btn btn-outline-dark btn-radius70"> อ่านเพิ่ม  </button></a>
					</div>


				</div>
				<div class="project_sl d-flex align-items-center">

					<div style="" class="owl-carousel owl-theme owl-loaded ">
						<div class="owl-stage-outer">
							<div class="owl-stage">
								<div class="owl-item">
									<div class="project_sl_img" style="background-image: url('assets/images/main/sl-project-1.png');" ></div>
									<!-- <img style="" class="project_sl_img img-responsive" src="assets/images/main/sl-project-1.png" > -->
								</div>
								<div class="owl-item">
									<div class="project_sl_img" style="background-image: url('assets/images/main/sl-project-2.png');"></div>
									<!-- <img style="" class="project_sl_img img-responsive" src="assets/images/main/sl-project-2.png" > -->
								</div>
								<div class="owl-item">
									<div class="project_sl_img" style="background-image: url('assets/images/main/sl-project-3.png');" ></div>
									<!-- <img style="" class="project_sl_img img-responsive" src="assets/images/main/sl-project-3.png" > -->
								</div>
								<div class="owl-item">
									<div class="project_sl_img" style="background-image: url('assets/images/main/sl-project-6.png');" ></div>
									<!-- <img style="" class="project_sl_img img-responsive" src="assets/images/main/sl-project-4.png" > -->
								</div>
							</div>

						</div>

					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid ">
	<div class="session news">
		<div class="full-HD">
			<div class="row news-content">
				<h1 class="dtac-bold oversize-l">ข่าวและกิจกรรมของเรา</h1>
				<div class="owl-carousel owl-theme owl-loaded">
					<div class="owl-stage-outer">
						<div class="owl-stage">
							<?php 
							for ($i=1; $i < 7; $i++) { 
								?>
								<div class="owl-item">
									<a target="_bank" href="https://www.brandbuffet.in.th/2016/07/dtac-fuk-fun-fest/">
										
										<div class="card col">
											<img class="card-img-top"  src="assets/images/main/sl_<?php echo $i ?>.png" alt="Card image cap">
											<div class="card-body">
												<p class="card-text">dtac ฟักฝันเฟส จัดงานเทศกาลแนะแนว
													อาชีพและสร้างแรงบันดาลใจสำหรับเด็ก
												ม.1-6 ที่ใหญ่ที่สุดในประเทศเพื่อให้...</p>
											</div>
										</div>

									</a>
								</div>
								<?php
							}
							?>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>

<div class="session-contact">
	<div class="container-fluid ">
		<div class="container">
			<div class="row">
				<div class="col">
					<h1 class="dtac-bold oversize-l text-center">ติดต่อเรา</h1>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="container">

		<div class="parallax text-center">
			<div id="particles-js" class="d-flex align-items-center">

				<div class="row contact-img" >

					<div class="text-center col-6 col-sm-3">
						<a href=""><img class="card-img-top " style="width: 50px; height: 50px;" src="assets/images/main/facebook.png" alt="Card image cap"></a>
					</div>
					<div class="text-center col-6 col-sm-3">
						<a href=""><img class="card-img-top " style="width: 50px; height: 50px;" src="assets/images/main/youtrub.png" alt="Card image cap"></a>
					</div>
					<div class="text-center col-6 col-sm-3">
						<a href=""><img class="card-img-top " style="width: 50px; height: 50px;" src="assets/images/main/tw.png" alt="Card image cap"></a>
					</div>
					<div class="text-center col-6 col-sm-3">
						<a href=""><img class="card-img-top " style="width: 50px; height: 50px;" src="assets/images/main/ig.png" alt="Card image cap"></a>
					</div>

				</div>

			</div>
		</div>


	</div>
</div>


<?php
$this->load->view('demo/_totop');
$this->load->view('demo/_footer');
$this->load->view('demo/_script');
$this->load->view('demo/main_script/script');
$this->load->view('demo/owl');
$this->load->view('demo/particles');
$this->load->view('demo/topic-box');
$this->load->view('demo/greensock');
$this->load->view('demo/scrollTop');
$this->load->view('demo/youtube');
?>