<div id="topY"></div>
<nav class="navbar navbar-expand-lg navbar-light bg-light bg-transparent" id="main-nav">
	<a class="navbar-brand">
		<img class="card-img-top img-fluid" src="assets/images/logo.png" srcset="assets/images/logo.png 1x, assets/images/logo@2x.png 2x" alt="dtac sustainability">
	</a>
	<button class="navbar-toggler" data-target="#my-nav" onclick="myFunction(this)" data-toggle="collapse">
		<span class="bar1"></span>
		<span class="bar2"></span>
		<span class="bar3"></span>
	</button>

	<div id="my-nav" class="collapse navbar-collapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a class="nav-link <?php if($page == 'demo'){ echo 'active'; } ?>" href="<?php echo base_url('Demo') ?>">ภาพรวม</a></li>
			<li class="nav-item"><a class="nav-link <?php if($page == 'service'){ echo 'active'; } ?>" href="<?php echo base_url('service') ?>">ความยั่งยืนของดีแทค</a></li>
			<li class="nav-item"><a class="nav-link <?php if($page == 'policy'){ echo 'active'; } ?>" href="<?php echo base_url('Policy') ?>">เราคิดอย่างไร</a></li>
			
			
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">สิ่งที่เราทำ</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo base_url('Project') ?>">ภาพรวมโครงการ</a>
					<a class="dropdown-item" href="<?php echo base_url('Project') ?>">โครงการ Smart Farmer</a>
					<a class="dropdown-item" href="<?php echo base_url('Project') ?>">การบริหารห่วงโซ่อุปทานยั่งยืน</a>
					<a class="dropdown-item" href="<?php echo base_url('Project') ?>">โครงการ Safe Internet</a>
					<a class="dropdown-item" href="<?php echo base_url('Project') ?>">โครงการเน็ตอาสา</a>
				</div>
			</li>
			

			<li class="nav-item"><a class="nav-link <?php if($page == 'contact'){ echo 'active'; } ?>" href="#contact">จากซีอีโอถึงคุณ</a></li>
			<li class="nav-item"><a class="nav-link <?php if($page == 'contact'){ echo 'active'; } ?>" href="#contact">SD Report</a></li>
			<li class="nav-item"><a class="nav-link <?php if($page == 'contact'){ echo 'active'; } ?>" href="#contact">รู้จักเรา</a></li>
			<li class="nav-item"><a class="nav-link <?php if($page == 'contact'){ echo 'active'; } ?>" href="#contact">ติดต่อเรา</a></li>
		</ul>
	<form>
		<ul class="nav navbar-nav navbar-right">
			<li><a class="active" href="terms.php">English</a><br><a href="condition.php">ไทย</a></li>
			<li id="main-nav-btn " style="margin-top: 10px;"><i class="fas fa-search"></i></li>
		<ul>
	</form>
		</div>
	</nav>


