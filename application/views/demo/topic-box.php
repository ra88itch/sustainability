<script type="text/javascript">
	
	$(".menu_zone .col-lg-4").on("mouseenter", function(e) {
		$(this).find(".icon").addClass("icon_hover");
		$(this).find(".show1").hide();
		$(this).find(".show2").show();
	}).on("mouseleave", function(e) {
		$(this).find(".icon").removeClass("icon_hover");
		$(this).find(".show1").show();
		$(this).find(".show2").hide();
	});

</script>