<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-3.3.1.slim.min.js"></script>
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<!-- owl carousel js-->
<script src="assets/third-party/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/js/main.js"></script>


<script src="assets/js/particles.js"></script>

<script type="text/javascript" src="assets/js/greensock/plugins/CSSPlugin.min.js"></script>
<script type="text/javascript" src="assets/js/greensock/TweenLite.min.js"></script>
<script type="text/javascript" src="assets/js/greensock/TweenMax.min.js"></script>

</body>
</html>