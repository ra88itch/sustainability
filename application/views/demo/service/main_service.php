<?php
$this->load->view('demo/_head');
$this->load->view('demo/_nav');
?>

<div class="session banner">
	<div class="full-HD">	
		<div class="container-fluid ">

			<div class="row">
				<div class="col-xl-6 col-lg-12 banner-content d-flex align-items-center">

					<div>
						<h1 class="dtac-bold oversize-xxl">ความยั่งยืน<br>ของดีแทค</h1>
						<p>สร้างความยั่งยืนให้แก่ประเทศชาติ เสริมสร้างรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร</p>
					</div>


				</div>
				<div class="col-xl-6 col-lg-12 banner-img">
					<div class="card"><img class="" src="assets/images/service/banner.png" alt=""></div>
				</div>

			</div>

		</div>
	</div>
</div>


<div class="session">
	<div class="container-fluid session-content-bg">
		<div class="">
			<div class="row content-box">
				<div class="offset-md-1 col-md-10 text-center content-backgroup">
					<p>ดีแทคได้ยึดถือหลักการ ‘ทำในสิ่งที่ถูกต้อง’ หรือ Do the Right Thing ควบคู่ไปกับการ ‘สร้างคุณค่าร่วม’ หรือ Creating Shared Value (CSV) ให้แก่สังคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค ‘สร้างไทยให้แกร่ง’ และการมุ่งสู่เป้าหมายการพัฒนาอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="session youtube">
	<div class="container-fluid ">
		<div class="full-HD">	
			<div class="row">
				<div class="youtube-video-content-one-video d-flex align-items-center">

					<div class="content">
						<h1 class="dtac-bold oversize-l">ความยั่งยืนของดีแทค</h1>

						<p>
							ประเทศไทยถือเป็นประเทศเศรษฐกิจเกิดใหม่ที่พรั่งพร้อมด้วยโอกาสทางธุรกิจสำหรับทุกๆ อุตสาหกรรมให้สามารถเติบโตได้อย่างไม่หยุดยั้ง อย่างไรก็ตาม ไม่สามารถปฏิเสธได้ว่า การพัฒนาอย่างรวดเร็วกลับนำมาซึ่งความเสี่ยงในด้านต่างๆ ไม่ว่าจะเป็นทรัพยากรที่หมดไป และการขาดแรงงานที่มีทักษะใหม่ๆ ซึ่งล้วนแล้วแต่สามารถทำให้การดำเนินธุรกิจต้องหยุดชะงักลงได้
						</p>

						<p>
							ในการดำเนินงานระยะยาวโดยองค์กรเพื่อสร้างความยั่งยืนให้แก่ประเทศชาตินั้น ดีแทคได้ยึดถือหลักการ ‘ทำในสิ่งที่ถูกต้อง’ หรือ Do the Right Thing ควบคู่ไปกับการ ‘สร้างคุณค่าร่วม’ หรือ Creating Shared Value (CSV) ให้แก่สังคมไทย รวมไปถึงผู้มีส่วนได้ส่วนเสียกลุ่มต่างๆ ซึ่งเป็นไปตามวิสัยทัศน์ของดีแทค ‘สร้างไทยให้แกร่ง’ และการมุ่งสู่เป้าหมายการพัฒนาอย่างยั่งยืนของสหประชาชาติ ข้อที่ 10 การลดความเหลื่อมล้ำทั้งในและนอกประเทศ
						</p>


					</div>


				</div>

				<div class="youtube-video-list">

					<iframe id="video" width="100%" loading="lazy" height="450px" src="https://www.youtube.com/embed/nfQrjutRptc" frameborder="0" loading="lazy" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>

			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="full-HD">	
			<div class="row ">
				<div class="youtube-video-content-one-video-underline  d-flex align-items-center">
					<div class="content-underline">
						<P>
							เป้าหมายในระยะยาวของดีแทค คือได้รับการยกย่องจากนักลงทุน หน่วยงานกำกับดูแล และจากกลุ่มผู้มีส่วนได้ส่วนเสียต่างๆ ให้เป็นองค์กรด้านโทรคมนาคมที่มีการบริหารจัดการเพื่อมุ่งสู่ความยั่งยืนในทุกๆ ด้าน โดยสามารถสร้างคุณค่าให้แก่การพัฒนาเศรษฐกิจและสังคมของประเทศชาติควบคู่ไปด้วย  ดังนั้นเป้าหมายในระยะกลางของดีแทค จึงเน้นที่การเสริมรากฐานการดำเนินงานด้านความยั่งยืนให้แข็งแกร่งทั่วทั้งองค์กร เช่น การจัดให้มีระบบติดตามและรายงานผลที่มีประสิทธิภาพ เป็นต้น ซึ่งจำเป็นต้องมีการประเมินความเสี่ยงและโอกาสในประเด็นต่างๆ ที่เกี่ยวข้องกับความยั่งยืนอย่างน้อยปีละ 1 ครั้ง”
						</P>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>



<!-- <div class="full-HD"> -->
	<div class="session policy">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 col-lg-6 text-center">
					<img src="assets/images/service/img2.png" id="img-policy" class="img-responsive" alt="">
				</div>
				<div class="col-xl-6 col-lg-6">
					<div class="row">
						<div class="col-xl-12 col-lg-12">

							<h1 class="dtac-bold oversize-l">นโยบายด้านความยั่งยืน</h1>

							<p>
								ดีแทคได้จำทำนโยบายการดำเนินงานด้านความยั่งยืน โดยใช้มาตรฐานระดับสากล อาทิ UN Global Compact, UN Universal Declaration of Human Rights, ILO Core Conventions เป็นต้น ร่วมกับการศึกษาประเด็นด้านความยั่งยืนที่สำคัญกับกลุ่มผู้มีส่วนได้ส่วนเสีย (stakeholders) ในการจัดทำนโยบายดังกล่าว เพื่อให้การดำเนินธุรกิจของบริษัทสามารถสร้างผลกระทบเชิงบวกทางเศรษฐกิจ สังคม และสิ่งแวดล้อมได้อย่างแท้จริง
							</p>

							<p>
								หลักการปฏิบัติที่สำคัญในนโยบายการดำเนินงานด้านความยั่งยืน ดังต่อไปนี้
							</p>

						</div>		

						<div class="min-session col-xl-12 col-lg-12">

							<div class="row list d-flex align-items-center">
								<p class="num-list">1</p>
								<p class="col content-list">
									ประเด็นเรื่องความยั่งยืนจะต้องครอบคลุมทั้งกระบวนการดำเนินธุรกิจของบริษัท
								</p>
							</div>
							<div class="row list d-flex align-items-center">
								<p class="num-list ">2</p>
								<p class="col content-list">
									ใช้หลักการการดำเนินธุรกิจอย่างมีความรับผิดชอบ (Responsible Business Practice) และความโปร่งใส
								</p>
							</div>
							<div class="row list d-flex align-items-center">
								<p class="num-list ">3</p>
								<p class="col content-list">
									การดำเนินธุรกิจหลัก (core business) ที่มุ่งสร้างประโยชน์และผลกระทบเชิงบวกแก่สังคม
								</p>
							</div>
							<div class="row list d-flex align-items-center">
								<p class="num-list ">4</p>
								<p class="col content-list">
									จะต้องมีการประเมินความเสี่ยงและโอกาสที่เกี่ยวข้องกับประเด็นความยั่งยืนตามระยะเวลาที่เหมาะสม
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- </div> -->

	<div class="session policy">
		<div class="container">
			<div class="row">
				<div class="content-interactive-head">
					<h1 class="dtac-bold oversize-l">กลยุทธ์ด้านความยั่งยืน</h1>

				</div>
			</div>

			<div class="row min-session show-PC">
				<div class="col-xl-4 col-lg-4">

					<div class="row">
						<div class="col  interactive interactive-active" name='1'>
							1.ทำในสิ่งที่ถูกต้อง (Do the Right Thing)
						</div>
					</div>
					<div class="row">
						<div class="col  interactive interactive-list" name='2'>
							2.สร้างศักยภาพแก่สังคม (Empower Societies)
						</div>
					</div>

				</div>
				<div class="col-xl-8 col-lg-8">

					<div class="content-interactive-lg" id="content_1">
						<h5>1.	ทำในสิ่งที่ถูกต้อง (Do the Right Thing)</h5>
						<p>
							ดีแทคให้ความสำคัญต่อการต่อต้านการทุจริตคอร์รัปชันทุกรูปแบบ รวมถึงการกระทำที่ผิดกฎหมายและจรรยาบรรณทางธุรกิจ พร้อมกับกำหนดนโยบายและแนวปฏิบัติในการคุ้มครองข้อมูลส่วนตัวและการบริหารห่วงโซ่อุปทานให้เป็นไปตามกฎหมายและตามมาตรฐานสากล นอกจากนี้ ดีแทคยังส่งเสริมให้มีการแข่งขันที่เป็นธรรม ร่วมด้วยการตรวจประเมินด้านสิทธิมนุษยชน (Human Rights Due Diligence) เพื่อลดความเสี่ยงที่อาจเกิดขึ้นจากการดำเนินธุรกิจ
						</p>

						<p>
							การดำเนินธุรกิจบนพื้นฐานของความถูกต้องและโปร่งใสนี้ได้สร้างคุณค่าให้แก่กลุ่มผู้มีส่วนได้ส่วนเสียทุกกลุ่มของดีแทค ยกตัวอย่างเช่น กลุ่มบริษัทคู่ค้าที่ผ่านการคัดเลือกและได้รับการประเมินผลตามกระบวนการที่โปร่งใสชัดเจน และมีการทำธุรกิจร่วมกันอย่างเป็นธรรม ส่วนกลุ่มหน่วยงานภาครัฐและกลุ่มหน่วยงานกำกับดูแลสามารถมั่นใจได้ว่าการทำงานร่วมกับดีแทคจะเป็นไปอย่างโปร่งใส ปราศจากการติดสินบนหรือการโน้มน้าวอย่างผิดจรรยาบรรณ ซึ่งอาจก่อให้เกิดความเสียหายต่อรัฐได้ ตลอดจนสังคมไทยที่จะได้รับประโยชน์จากการขยายโครงข่ายการสื่อสารอย่างเต็มเม็ดเต็มหน่วย เพิ่มขีดความสามารถในการแข่งขันของประเทศในระดับภูมิภาค
						</p>

					</div>

					<div class="content-interactive-lg" id="content_2" style="display: none;">
						<h5>2.สร้างศักยภาพแก่สังคม (Empower Societies)</h5>

					</div>

				</div>

			</div>

			<div class="show-M">
				<div class="content-interactive-m">
					<h5>1.	ทำในสิ่งที่ถูกต้อง (Do the Right Thing)</h5>
					<p>
						ดีแทคให้ความสำคัญต่อการต่อต้านการทุจริตคอร์รัปชันทุกรูปแบบ รวมถึงการกระทำที่ผิดกฎหมายและจรรยาบรรณทางธุรกิจ พร้อมกับกำหนดนโยบายและแนวปฏิบัติในการคุ้มครองข้อมูลส่วนตัวและการบริหารห่วงโซ่อุปทานให้เป็นไปตามกฎหมายและตามมาตรฐานสากล นอกจากนี้ ดีแทคยังส่งเสริมให้มีการแข่งขันที่เป็นธรรม ร่วมด้วยการตรวจประเมินด้านสิทธิมนุษยชน (Human Rights Due Diligence) เพื่อลดความเสี่ยงที่อาจเกิดขึ้นจากการดำเนินธุรกิจ
					</p>

					<p>
						การดำเนินธุรกิจบนพื้นฐานของความถูกต้องและโปร่งใสนี้ได้สร้างคุณค่าให้แก่กลุ่มผู้มีส่วนได้ส่วนเสียทุกกลุ่มของดีแทค ยกตัวอย่างเช่น กลุ่มบริษัทคู่ค้าที่ผ่านการคัดเลือกและได้รับการประเมินผลตามกระบวนการที่โปร่งใสชัดเจน และมีการทำธุรกิจร่วมกันอย่างเป็นธรรม ส่วนกลุ่มหน่วยงานภาครัฐและกลุ่มหน่วยงานกำกับดูแลสามารถมั่นใจได้ว่าการทำงานร่วมกับดีแทคจะเป็นไปอย่างโปร่งใส ปราศจากการติดสินบนหรือการโน้มน้าวอย่างผิดจรรยาบรรณ ซึ่งอาจก่อให้เกิดความเสียหายต่อรัฐได้ ตลอดจนสังคมไทยที่จะได้รับประโยชน์จากการขยายโครงข่ายการสื่อสารอย่างเต็มเม็ดเต็มหน่วย เพิ่มขีดความสามารถในการแข่งขันของประเทศในระดับภูมิภาค
					</p>

				</div>

				<div class="content-interactive-m ">
					<h5>2.สร้างศักยภาพแก่สังคม (Empower Societies)</h5>

				</div>
			</div>
		</div>
	</div>




	<div class="session ">
		<div class="container-fluid feature-area">
			<div class="container">
				<div class="row ">
					<div class="offset-md-1 col-md-10 text-center ">
						<h1 class="dtac-bold oversize-l">เอกสารเผยแพร่</h1>
						<p>ท่านสามารถดาวน์โหลดเอกเอกสารนโยบายของดีแทค ได้จาก Link ด้านล่างนี้ </p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="session download">
		<div class="container-fluid feature-area">
			<div class="container">
				<div class="row ">

					<?php 
					for ($i=0; $i < 3; $i++) { 
						?>
						<div class="col-xl-3 col-lg-3">
							<div class="download-bg">

								<div class="show_1 text-left ">


									<h6><strong> นโยบายและกลยุทธ์ด้านความยั่งยืน </strong></h6>
									<p>เอกสารเผยแพร่</p>
									<h6>PDF 6.8 MB</h6>


								</div>

								<div class="show_2" style="display: none;">
									<div class="">

										<div class="text-center">
											<img src="assets/images/service/download.png" alt="">
											<h6>PDF 6.8 MB</h6>
										</div>

									</div>	
								</div>

							</div>
						</div>
						<?php
					}
					?>

				</div>
			</div>
		</div>
	</div>


	<?php
	$this->load->view('demo/_totop');
	$this->load->view('demo/_footer');
	$this->load->view('demo/_script');
	$this->load->view('demo/scrollTop');
	$this->load->view('demo/interactive');
	$this->load->view('demo/download');
	?>