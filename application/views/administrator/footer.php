</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?php echo $root; ?>assets/administrator/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $root; ?>assets/administrator/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo $root; ?>assets/administrator/js/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo $root; ?>assets/administrator/js/raphael.min.js"></script>
<script src="<?php echo $root; ?>assets/administrator/js/morris.min.js"></script>
<script src="<?php echo $root; ?>assets/administrator/js/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo $root; ?>assets/administrator/js/sb-admin-2.js"></script>

</body>

</html>
